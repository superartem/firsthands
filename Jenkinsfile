pipeline {
    agent any
    stages {
        stage("verify tooling") {
            steps {
            sh '''
                docker version
                docker info
                docker compose version 
                pwd
                curl --version
            '''
            }
        }
        stage('Configure secrets') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab_secrets', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh ''' 
                            rm -rf secrets_firsthands
                            sudo chmod 777 -R keys || true
                            rm -rf ./keys
                            git clone https://$USERNAME:$PASSWORD@gitlab.com/superartem/secrets_firsthands 
                            mv -f secrets_firsthands/keys ./
                            docker build -t cert secrets_firsthands
                            rm -rf secrets_firsthands
                        '''
                    }
                }
            }
        }
        stage('Login') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'dockerhub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh 'echo $PASSWORD | docker login -u $USERNAME --password-stdin'
                    }
                }
            }
        }
        stage('Build containers') {
            steps {
                sh 'docker compose -f production.yml build'
                sh 'docker compose -f production.yml push'
            }
        }
        stage('Start container') {
            steps {
                sh 'docker compose -f production.yml up -d'
                sh 'docker compose ps'
            }
        }
        stage('Run certbot') {
            steps {
                sh '''
                    docker stop certbot || true
                    docker rm certbot || true
                    docker run -d --name certbot --restart always \
                        -v "/root/jenkins_home/workspace/pershiruki/keys:/etc/letsencrypt" \
                        -v "/root/jenkins_home/workspace/pershiruki/nginx/certbot:/var/lib/letsencrypt" \
                        -v "/var/run/docker.sock:/var/run/docker.sock" \
                        cert
                    docker exec certbot chmod -R 777 /var/lib/letsencrypt
                '''
            }
        }
    }
    post {
        always {
            sh 'docker compose logs'
            sh 'docker compose ps'
        }
    }
}
