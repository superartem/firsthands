from app.database import Base
from sqlalchemy import Column, Integer, String, DateTime



class Funding(Base):
    __tablename__ = 'fundings'

    id = Column(String, primary_key=True)
    creator = Column(String)
    signature = Column(String)
    title = Column(String)
    description = Column(String)
    file = Column(String)
    created_at = Column(DateTime)