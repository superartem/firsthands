import random
import json 
import os
import sys

from hashlib import sha256
from typing import Union, Annotated
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware

from fastapi import FastAPI, File, Form, UploadFile, Depends
from web3 import Web3
from hexbytes import HexBytes
from eth_account.messages import encode_defunct

import app.models as models
from app.database import engine, SessionLocal, get_db
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import Session


app = FastAPI()
@app.on_event("startup")
def on_startup():
    models.Base.metadata.create_all(bind = engine)

w3 = Web3(Web3.HTTPProvider("https://sepolia.infura.io/v3/6dcad30b0dea43caa4ca6308e65b2e9e"))

@app.post("/fundings/")
def create_funding(
    message: str = Form(...),
    signature: str = Form(...),
    tx: str = Form(...),
    file: UploadFile = File(...),
    db: Session = Depends(get_db)
):
    obj = json.loads(message)
    data = w3.eth.get_transaction_receipt(tx)
    msg = encode_defunct(text=message)
    verify_address = w3.eth.account.recover_message(msg,signature=HexBytes(signature))
    if (obj["creator"] == verify_address.lower() == data["from"].lower()) and (data["contractAddress"] == obj["contract"]):
        ext = file.filename.split('.')[1]
        new_filename = sha256(bytes(
            file.filename + str(random.randint(-169995342, 123912939)), "UTF-8")).hexdigest() + "." + ext
        
        file_location = f"./storage/{new_filename}"
        with open(file_location, "wb+") as file_object:
            file_object.write(file.file.read())
        filename = f"/storage/{new_filename}"
        new_funding = models.Funding(
            id = obj["contract"],
            creator = obj["creator"],
            signature = signature,
            title = obj["title"],
            description = obj["description"],
            file = filename
        )
        
        db.add(new_funding)
        db.commit()
        el = db.query(models.Funding).where(models.Funding.id == obj["contract"])[0]
        return el
    else:
        return {"result": "could not verify signature"}


@app.get("/storage/{item}")
def get_image(item: str):
    return FileResponse(f"./storage/{item}")

@app.get("/fundings")
def get_fundings(
    db: Session = Depends(get_db)
):
    fundings = []
    res = db.query(models.Funding).order_by(desc(models.Funding.created_at))
    for instance in res:
        if not w3.eth.get_code(instance.id):
            db.query(models.Funding).filter(models.Funding.id==instance.id).delete()
            os.remove(f'.{instance.file}')
            db.commit()
        else:
            fundings.append(instance)
    
    return fundings

@app.get("/fundings/{item}")
def get_funding(item: str, db: Session = Depends(get_db)):
    fund = db.query(models.Funding).where(models.Funding.id == item).first()
    if fund:
        code = w3.eth.get_code(item)
        if code:
            return fund
        else:   # funding doesn't exist anymore
            db.delete(fund)
            os.remove(f'.{fund.file}')
            db.commit()
            return {"deleted": True}
    else:
        return {"deleted": True}

@app.get("/exists/{item}")
def exists_funding(item: str, db: Session = Depends(get_db)):
    fund = db.query(models.Funding).where(models.Funding.id == item).first() 
    if fund:
        code = w3.eth.get_code(item)
        if code:
            return True
        else:   # funding doesn't exist anymore
            db.delete(fund)
            os.remove(f'.{fund.file}')
            db.commit()
            return False
    else:
        return False