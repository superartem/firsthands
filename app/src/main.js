import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import './style.css'
import './index.css'
import router from './router'
import App from './App.vue'

import messages from '@intlify/unplugin-vue-i18n/messages'
const i18n = createI18n({
  locale: "en",
  messages: messages,
})

createApp(App)
    .use(router)
    .use(i18n)
    .mount('#app')

