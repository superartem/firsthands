import { createRouter, createWebHistory } from 'vue-router'
import Main from './pages/Main.vue'
import Funding from './pages/Funding.vue'
import About from './pages/About.vue'
import Donate from './pages/Donate.vue'

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: Main,
    },
    {
      path: '/funding/:id',
      component: Funding,
    },
    {
      path: '/about',
      component: About,
    },
    {
      path: '/donate',
      component: Donate,
    }
  ]
})
