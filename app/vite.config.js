import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueI18n from '@intlify/unplugin-vue-i18n/vite'
import * as path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueI18n({
      include: path.resolve(__dirname, "./src/locales/**")
    })],
  compilerOptions: {
    "types": ["@intlify/unplugin-vue-i18n/messages"]
  },
  build: {
    outDir: path.join(__dirname, '../dist')
  }
})
