// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

contract Funding {

    address payable public owner;
    uint public expired_datetime;
    uint public required_funds;

    address payable[] public donators;
    mapping (address => uint) public donate_amounts;

    constructor(uint duration_secs, uint required_wei) {
        owner = payable(msg.sender);
        expired_datetime = block.timestamp + duration_secs;
        required_funds = required_wei;
    }

    function returnFunds() public {
        require(block.timestamp > expired_datetime, "Funding must be expired");
        for (uint i = 0; i < donators.length; i++) {
            donators[i].transfer(donate_amounts[donators[i]]);
        }
        selfdestruct(owner);
    }
    function getExpiry() public view returns (uint) {
        return expired_datetime;
    }
    function getRequiredFunds() public view returns (uint) {
        return required_funds;
    }
    function getDonatorsCount() public view returns (uint) {
        return donators.length;
    }
    function totalfunds() public view returns (uint) {
        return address(this).balance;
    }
    function enoughFunds() public {
        if (block.timestamp > expired_datetime) {
            this.returnFunds();
        } else if (address(this).balance >= required_funds) {
            selfdestruct(owner);
        } 
    }
    function donate() public payable {
        if (donate_amounts[msg.sender] > 0) {
            donate_amounts[msg.sender] += msg.value;
        } else {
            donate_amounts[msg.sender] = msg.value;
        }

        bool found = false;
        for (uint i=0; i < donators.length; i++) {
            if (donators[i] == msg.sender){
                found=true;
                break;
            }
        }
        if (!found) {
            donators.push(payable(msg.sender));
        }
        
        this.enoughFunds();
    }

}
